package proj.simplon.co.promo16.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import proj.simplon.co.promo16.entity.Medecin;

public class MedecinRepoTest {
    private MedecinRepo medecinRepo;

    @Before
    public void setUp() {
        medecinRepo = new MedecinRepo();
        medecinRepo.getConnection();
    }

    @Test
    public void testFindAll() {
        List<Medecin> result = medecinRepo.findAll();
        assertEquals(4, result.size());
    }

    @Test
    public void testSave() {
        Medecin toAdd = new Medecin("test", "test");
        medecinRepo.save(toAdd);

        assertNotNull(toAdd.getId());
    }

    @Test
    public void testFindById() {
        Medecin medecin = medecinRepo.findById(1);
        assertTrue(medecin.getId() == 1);
    }

}
