package proj.simplon.co.promo16.repository;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import proj.simplon.co.promo16.entity.Patient;


public class PatientRepoTest {

    private PatientRepo repo;

    @Before
    public void setUp() {
        repo = new PatientRepo();
        repo.getConnection();
    }

    @Test
    public void testFindAll() {
        List<Patient> result = repo.findAll();
        assertEquals(3, result.size());
    }

    @Test
    public void testFindById() {
        Patient toFind = repo.findById(2);
        assertTrue(toFind.getId()==2);
    }

    @Test
    public void testSave() {
        Patient toAdd = new Patient("test","test","test");
        repo.save(toAdd);
        assertNotNull(toAdd.getId());
    }
}
