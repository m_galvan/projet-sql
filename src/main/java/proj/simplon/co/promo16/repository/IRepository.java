package proj.simplon.co.promo16.repository;

import java.util.List;

public interface IRepository<T> {
    List<T> findAll();
    T findById(Integer id);
    boolean save(T entity);
    boolean update(T entity);
    boolean deleteById(Integer id);
}