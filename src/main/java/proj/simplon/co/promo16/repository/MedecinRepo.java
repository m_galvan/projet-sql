package proj.simplon.co.promo16.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import proj.simplon.co.promo16.entity.Medecin;

public class MedecinRepo implements IRepository<Medecin> {

    private Connection connection;

    public MedecinRepo() {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://simplon:1234@localhost:3306/allodocdb");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Medecin> findAll() {

        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM medecin");
            ResultSet result = stmt.executeQuery();
            List<Medecin> medecinList = new ArrayList<>();

            while (result.next()) {
                Medecin medecin = new Medecin(
                        result.getInt("medecin_id"),
                        result.getString("first_name"),
                        result.getString("last_name"));
                medecinList.add(medecin);
            }
            return medecinList;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Medecin findById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM medecin WHERE medecin_id= ?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new Medecin(
                        result.getInt("medecin_id"),
                        result.getString("first_name"),
                        result.getString("last_name"));
            }

        } catch (SQLException e) {

        }

        return null;
    }

    @Override
    public boolean deleteById(Integer id) {
        try {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM medecin WHERE medecin_id=?");
            stmt.setInt(1, id);
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;

    }

    @Override
    public boolean save(Medecin entity) {
        try {
            if (entity.getId() != null) {
                return update(entity);
            }
            PreparedStatement stmt = connection
                    .prepareStatement("INSERT INTO medecin (first_name,last_name) VALUES (?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, entity.getFirstName());
            stmt.setString(2, entity.getLastName());

            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                entity.setId(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;

    }

    @Override
    public boolean update(Medecin entity) {
        try {
            PreparedStatement stmt = connection
                    .prepareStatement("UPDATE medecin SET first_name=?,last_name= ? WHERE medecin_id=?");
            stmt.setString(1, entity.getFirstName());
            stmt.setString(2, entity.getLastName());
            stmt.setInt(3, entity.getId());
            return stmt.executeUpdate() == 1;
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return false;

    }

    public void getConnection() {
    }
}
