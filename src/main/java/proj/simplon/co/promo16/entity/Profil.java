package proj.simplon.co.promo16.entity;

public class Profil {
    private Integer id;
    private String observation;
    private String[] ordonnance;

    public Profil() {
    }

    public Profil(String observation, String[] ordonnance) {
        this.observation = observation;
        this.ordonnance = ordonnance;
    }

    public Profil(Integer id, String observation, String[] ordonnance) {
        this.id = id;
        this.observation = observation;
        this.ordonnance = ordonnance;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public String[] getOrdonnance() {
        return ordonnance;
    }

    public void setOrdonnance(String[] ordonnance) {
        this.ordonnance = ordonnance;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
