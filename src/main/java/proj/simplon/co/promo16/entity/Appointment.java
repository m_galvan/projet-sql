package proj.simplon.co.promo16.entity;

import java.time.LocalDate;
import java.time.LocalTime;

public class Appointment {
    private Integer id;
    private String title;
    private String description;
    private boolean isActive;
    private LocalDate date;
    private LocalTime time;

    public Appointment(String title, String description, LocalDate date, LocalTime time) {
        this.title = title;
        this.description = description;
        this.isActive = false;
        this.date = date;
        this.time = time;
    }

    public Appointment(int id, String title, String description, LocalDate date, LocalTime time) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.isActive = false;
        this.date = date;
        this.time = time;

    }

    public Appointment(boolean isActive, LocalDate date, LocalTime time, String title, String description) {
        this.title = title;
        this.description = description;
        this.isActive = isActive;
        this.date = date;
        this.time = time;
        this.title = title;
    }

    public Appointment() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    @Override
    public String toString() {
        // return "Appointment [date=" + date + "\n, description=" + description + "\n,
        // id=" + id + "\n, isActive=" + isActive
        // + "\n, time=" + time + "\n, title=" + title + "]\n";

        return "\nRendez-vous n°" + id + "\n Titre : " + title + "\n Description : " + description
                + "\n Actif : " + isActive + "\n Date : " + date + "\n Heure : " + time + "\n";
    }

}
